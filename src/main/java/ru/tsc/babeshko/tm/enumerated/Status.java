package ru.tsc.babeshko.tm.enumerated;

public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    public static Status toStatus(final String value){
        if (value == null || value.isEmpty()) return null;
        for (final Status status: values()){
            if (status.name().equals(value)) return status;
        }
        return null;
    }

    private String displayName;

    Status(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}
